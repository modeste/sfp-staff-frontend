echo "Install Dependences..."
npm install
echo "Building SFP Staff project for production..."
npm run build
echo "Copying built files..."
rm -rf sfp-staff-prod/dist/
cp -R dist/ sfp-staff-prod/dist/
cd sfp-staff-prod
echo "Install Dependences..."
npm install
cd ..
echo "commit changes..."
git add .
git commit -m "commit changes..."
echo "Restart PM2...."
pm2 restart all
echo "Restart Nginx...."
service nginx restart

