# sfp-staff-frontend

``` bash
# Add backend URL to path ./src/config/index.js file

export default {
  serverURL: 'http://localhost:3000'
}
```
cd sfp-staff-prod
pm2 start index.js --name sfp-staff-frontend

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve -- --port 3001
```

### Compiles and minifies for production
```
npm run build
```

## Authors

1. Modeste Gougbedji <gougbedjimodeste@gmail.com>
