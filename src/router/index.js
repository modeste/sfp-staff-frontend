import Vue from 'vue'
import Router from 'vue-router'
import authGuard from './auth-guard'

Vue.use(Router)

const router = new Router({
	base: "/",
	mode: 'history',
	scrollBehavior() {
		return { x: 0, y: 0 }
	},
	routes: [
		{
			// =============================================================================
			// MAIN LAYOUT ROUTES
			// =============================================================================
			path: '',
			component: () => import('@/layouts/main/Main.vue'),
			children: [
				// =============================================================================
				// Theme Routes
				// =============================================================================
				{
					path: '/',
					name: 'Home',
					component: () => import('@/views/Home.vue'),
					meta: {
						auth: true,
						breadcrumb: [
							{ title: 'Accueil', active: true },
						],
						pageTitle: 'Accueil'
					},
				},
				{
					path: '/users',
					name: 'Users',
					component: () => import('@/views/sideBar/User'),
					meta: {
						auth: true,
						breadcrumb: [
							{ title: 'Utilisateurs', active: true },
						],
						pageTitle: 'Utilisateurs'
					},
				},
				{
					path: '/companies',
					name: 'Companies',
					component: () => import('@/views/sideBar/Company'),
					meta: {
						auth: true,
						breadcrumb: [
							{ title: 'Filiales', active: true },
						],
						pageTitle: 'Filiales'
					},
				},
				{
					path: '/workers',
					name: 'Workers',
					component: () => import('@/views/sideBar/Worker'),
					meta: {
						auth: true,
						breadcrumb: [
							{ title: 'Employé(e)s', active: true },
						],
						pageTitle: 'Employé(e)s'
					},
				},
				{
					path: '/profil',
					name: 'Profil',
					component: () => import('@/views/afterAuth/Profil.vue'),
					meta: {
						auth: true,
						breadcrumb: [
							{ title: 'Accueil', name: 'Home' },
							{ title: 'Mon Profil', active: true },
						],
						pageTitle: 'Mon Profil'
					},
				},
			],
		},
		// =============================================================================
		// FULL PAGE LAYOUTS
		// =============================================================================
		{
			path: '',
			component: () => import('@/layouts/full-page/FullPage.vue'),
			children: [
				// =============================================================================
				// PAGES
				// =============================================================================
				{
					path: '/login',
					name: 'Login',
					component: () => import('@/views/beforeAuth/Login.vue'),
				},

				{
					path: '/forget-password',
					name: 'ForgetPassword',
					component: () => import('@/views/beforeAuth/ForgetPassword.vue'),
				},
				{
					path: '/error-404',
					name: 'Error404',
					component: () => import('@/views/beforeAuth/Error404.vue'),
				},
			]
		},
		// Redirect to 404 page, if no match found
		{
			path: '*',
			redirect: '/error-404'
		}
	],
})


router.beforeEach(authGuard)
router.afterEach(() => {
	// Remove initial loading
	const appLoading = document.getElementById('loading-bg')
	if (appLoading) {
		appLoading.style.display = "none";
	}
})
Vue.router = router

export default router
