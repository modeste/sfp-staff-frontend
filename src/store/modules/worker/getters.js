import { mixin } from "@/mixins/mixin";

export default {
  allWorkers: (state) => {
    return state.workers
  },

  currentWorkerData: (state) => {
    return state.currentWorker
  },
}
