


import {
  FETCH_WORKER,
  GET_WORKER,
  ADD_WORKER,
  UPDATE_WORKER,
  REMOVE_WORKER,
} from './mutation-types'

import service from '@/service'
import config from '@/config'
import axios from 'axios'

const $service = service(axios, config)

export default {
  async applyAddWorker({ commit }, data) {
    const item = await $service.addWorker(data)
    commit(ADD_WORKER, item.data)
  },
  async applyGetAllWorker({ commit }, data) {
    const items = await $service.getAllWorker(data)
    commit(FETCH_WORKER, items.data)
  },
  async applyGetWorker({ commit }, data) {
    const item = await $service.getWorker(data)
    commit(GET_WORKER, item.data)
  },
  async applyPutWorker({ commit }, data) {
    const item = await $service.putWorker(data)
    commit(UPDATE_WORKER, item.data)
  },
  async applyRemoveWorker({ commit }, data) {
    let promises = []
    let { ids } = data
    ids.forEach(function (id, index) {
      promises.push((async function (id) {
        await $service.removeWorker({
          workerId: id,
        })
        commit(REMOVE_WORKER, id)
      })(id))
    })
    await Promise.all(promises)
  },
}

