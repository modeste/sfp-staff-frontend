export const FETCH_WORKER = 'FETCH_WORKER'
export const ADD_WORKER = 'ADD_WORKER'
export const UPDATE_WORKER = 'UPDATE_WORKER'
export const REMOVE_WORKER = 'REMOVE_WORKER'
export const GET_WORKER = 'GET_WORKER'

