import {
  FETCH_WORKER,
  ADD_WORKER,
  UPDATE_WORKER,
  REMOVE_WORKER,
  GET_WORKER,
} from './mutation-types'

import Vue  from 'vue'

export default {
  [GET_WORKER](state, data) {
    state.currentWorker = data
  },
  [FETCH_WORKER](state, data) {
    state.workers = data
  },
  [ADD_WORKER](state, worker) {
    let workersList = [...state.workers.list]
    workersList.unshift(worker)
    state.workers.list = workersList
  },
  [UPDATE_WORKER](state, worker) {
    const index = state.workers.list.findIndex((el)=>el.id === worker.id)
    state.currentWorker = worker
    Vue.set(state.workers.list, index, worker)
  },
  [REMOVE_WORKER](state, id) {
    const index = state.workers.list.findIndex((el)=>el.id === id)
    state.workers.list.splice(index, 1);
  }
}
