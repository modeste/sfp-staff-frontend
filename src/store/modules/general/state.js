export default {
    userTypes: {
        'super_administrator_sys': 'Super Administrateur',
        'administrator_sys': 'Administrateur',
        'rh': "RH",
        'dsi': "DSI",
        'it': "IT",
        'da': "DA",
        'dg': "DG",
        'none': "Aucun",
    },
}
