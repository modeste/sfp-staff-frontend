export default {
    allUserTypes: state => {
        return state.userTypes
    },
    rowsTable: (state) => {
        return [10, 50, 100, "Tout"]
    },
    allUserAcces: state => {
        return {
          chk_company_create: false,
          chk_company_update: false,
          chk_company_delete: false,
          chk_worker_create: false,
          chk_worker_update: false,
          chk_worker_delete: false,
    
          isDisableCompanyCreate: false,
          isDisableCompanyUpdate: false,
          isDisableCompanyDelete: false,
          isDisableWorkerCreate: false,
          isDisableWorkerUpdate: false,
          isDisableWorkerDelete: false,
        }
      },
}
