import {
} from './mutation-types'

import service from '@/service'
import config from '@/config'
import axios from 'axios'

const $service = service(axios, config)

export default {
  async applyUploadFile ({commit}, data) {
    return await $service.uploadFile(data)
  },
  async applyStatsGeneral ({commit}, data) {
    const items = await $service.statsGeneral(data)
    return items.data
  },
}
