


import {
  FETCH_COMPANY,
  GET_COMPANY,
  ADD_COMPANY,
  UPDATE_COMPANY,
  REMOVE_COMPANY,
} from './mutation-types'

import service from '@/service'
import config from '@/config'
import axios from 'axios'

const $service = service(axios, config)

export default {
  async applyAddCompany({ commit }, data) {
    const item = await $service.addCompany(data)
    commit(ADD_COMPANY, item.data)
  },
  async applyGetAllCompany({ commit }, data) {
    const items = await $service.getAllCompany(data)
    commit(FETCH_COMPANY, items.data)
  },
  async applyGetCompany({ commit }, data) {
    const item = await $service.getCompany(data)
    commit(GET_COMPANY, item.data)
  },
  async applyPutCompany({ commit }, data) {
    const item = await $service.putCompany(data)
    commit(UPDATE_COMPANY, item.data)
  },
  async applyRemoveCompany({ commit }, data) {
    let promises = []
    let { ids } = data
    ids.forEach(function (id, index) {
      promises.push((async function (id) {
        await $service.removeCompany({
          companyId: id,
        })
        commit(REMOVE_COMPANY, id)
      })(id))
    })
    await Promise.all(promises)
  },
}

