import { mixin } from "@/mixins/mixin";

export default {
  allCompanies: (state) => {
    return state.companies
  },
 
  currentCompanyData: (state) => {
    return state.currentCompany
  },
}
