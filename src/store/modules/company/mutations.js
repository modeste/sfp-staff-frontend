import {
  FETCH_COMPANY,
  ADD_COMPANY,
  UPDATE_COMPANY,
  REMOVE_COMPANY,
  GET_COMPANY,
} from './mutation-types'

import Vue  from 'vue'

export default {
  [GET_COMPANY](state, data) {
    state.currentCompany = data
  },
  [FETCH_COMPANY](state, data) {
    state.companies = data
  },
  [ADD_COMPANY](state, company) {
    let companiesList = [...state.companies.list]
    companiesList.unshift(company)
    state.companies.list = companiesList
  },
  [UPDATE_COMPANY](state, company) {
    const index = state.companies.list.findIndex((el)=>el.id === company.id)
    state.currentCompany = company
    Vue.set(state.companies.list, index, company)
  },
  [REMOVE_COMPANY](state, id) {
    const index = state.companies.list.findIndex((el)=>el.id === id)
    state.companies.list.splice(index, 1);
  }
}
