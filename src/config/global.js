export default {
  selectNotFoundData: 'Aucun élément trouvé',
  selectLabel: "Choisir",
  selectedLabel: "Sélectionné",
  deselectLabel: "Désélectionner",
}

