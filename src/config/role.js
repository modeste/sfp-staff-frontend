export default {
  notAllowTitle: "Impossible",
  notAllowText: "Vous n'avez pas les droits d'effectuer cette action",
  allNotAllowText: "Cet élément est fourni en standard par SFP. Il n'est ni supprimable, ni modifiable.",

  super_administrator_sys: {
    // Super Administrateurs
    users: {
      create: true,
      update: true,
      delete: true,
      initialize: true, // Reinitialiser le mot de passe
      lock: true, // Activation / Desactivation
      all: true,
      show: true,
    },
  },
  administrator_sys: {
    // Administrateurs
    users: {
      create: false,
      update: false,
      delete: false,
      initialize: false, // Reinitialiser le mot de passe
      lock: false, // Activation / Desactivation
      all: false,
      show: false,
    },
  },
  rh: {
    // rh
    users: {
      create: false,
      update: false,
      delete: false,
      initialize: false, // Reinitialiser le mot de passe
      lock: false, // Activation / Desactivation
      all: false,
      show: false,
    },
  },
  dsi: {
    // dsi
    users: {
      create: false,
      update: false,
      delete: false,
      initialize: false, // Reinitialiser le mot de passe
      lock: false, // Activation / Desactivation
      all: false,
      show: false,
    },
  },
  it: {
    // it
    users: {
      create: false,
      update: false,
      delete: false,
      initialize: false, // Reinitialiser le mot de passe
      lock: false, // Activation / Desactivation
      all: false,
      show: false,
    },
  },
  da: {
    // da
    users: {
      create: false,
      update: false,
      delete: false,
      initialize: false, // Reinitialiser le mot de passe
      lock: false, // Activation / Desactivation
      all: false,
      show: false,
    },
  },
  dg: {
    // dg
    users: {
      create: false,
      update: false,
      delete: false,
      initialize: false, // Reinitialiser le mot de passe
      lock: false, // Activation / Desactivation
      all: false,
      show: false,
    },
  },
};
