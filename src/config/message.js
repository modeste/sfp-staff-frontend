export default {
  users: {
    add: {
      success: {
        color: "success",
        text: "Utilisateur ajouté avec succès",
        title: "Ajout"
      },
      error: {
        color: "danger",
        text: "Erreur lors de l'ajout de l'utilisateur",
        title: "Ajout"
      }
    },
    update: {
      success: {
        color: "success",
        text: "Utilisateur modifié avec succès",
        title: "Mis à jour"
      },
      error: {
        color: "danger",
        text: "Erreur lors de la modification de l'utilisateur",
        title: "Mis à jour"
      }
    },
    delete: {
      success: {
        color: "success",
        text: "Utilisateur  supprimé avec succès",
        title: "Suppression"
      },
      error: {
        color: "danger",
        text: "Erreur lors de la suppression de l'utilisateur",
        title: "Suppression"
      }
    },
    multipleDeletion: {
      success: {
        color: "success",
        text: "Utilisateurs  supprimés avec succès",
        title: "Suppression"
      },
      error: {
        color: "danger",
        text: "Erreur lors de la suppression des utilisateurs",
        title: "Suppression"
      }
    },
    active: {
      success: {
        color: "success",
        text: "Compte activé avec succès",
        title: "Activation"
      }
    },
    desactive: {
      success: {
        color: "success",
        text: " Compte désactivé avec succès",
        title: "Désactivation"
      }
    },
    reinitialise: {
      success: {
        color: "success",
        text: "Réinitialisé avec succès",
        title: "Mot de passe"
      },
      error: {
        color: "danger",
        text: "Utilisateur n'existe pas",
        title: "Mot de passe"
      }
    },
    mdp: {
      success: {
        color: "success",
        text: "Mot de passe a été changé avec succès",
        title: "Changement du mot de passe"
      },
      error: {
        color: "danger",
        text: "Ancien mot de passe incorrect",
        title: "Changement du mot de passe"
      }
    },
    updateUser: {
      success: {
        color: "success",
        text: "Les informations ont été mises à jour avec succès",
        title: "Mise à jour profil"
      },
      error: {
        color: "danger",
        text: "Une erreur s'est produite",
        title: "Mise à jour profil"
      }
    },
    updateAvatar: {
      success: {
        color: "success",
        text: "Image a été téléchargée avec succès",
        title: "Mise à jour avatar"
      },
      error: {
        color: "danger",
        text: "Une erreur s'est produite",
        title: "Mise à jour profil"
      }
    },
    notFound: {
      error: {
        color: "danger",
        text: "Le nom d'utilisateur n'existe pas",
        title: "Utilisateur"
      },
    },
    nameExist: {
      error: {
        color: "danger",
        text: "Nom d'utilisateur existe déjà",
        title: "Utilisateur"
      },
    },
    mailError: {
      error: {
        color: "danger",
        text: "Impossible d'envoyer le mail à l'adresse",
        title: "Mail"
      },
    },
  },
  company: {
    add: {
      success: {
        color: "success",
        text: "Filiale ajoutée avec succès",
        title: "Ajout"
      },
      error: {
        color: "danger",
        text: "Erreur lors de l'ajout de la filiale",
        title: "Ajout"
      }
    },
    update: {
      success: {
        color: "success",
        text: "Filiale modifiée avec succès",
        title: "Mis à jour"
      },
      error: {
        color: "danger",
        text: "Erreur lors de la modification de la filiale",
        title: "Mis à jour"
      }
    },
    delete: {
      success: {
        color: "success",
        text: "Filiale supprimée avec succès",
        title: "Suppression"
      },
      error: {
        color: "danger",
        text: "Erreur lors de la suppression de la filiale",
        title: "Suppression"
      }
    },
    multipleDeletion: {
      success: {
        color: "success",
        text: "Filiales supprimées avec succès",
        title: "Suppression"
      },
      error: {
        color: "danger",
        text: "Erreur lors de la suppression des filiales",
        title: "Suppression"
      }
    },
  },
  worker: {
    add: {
      success: {
        color: "success",
        text: "Filiale ajoutée avec succès",
        title: "Ajout"
      },
      error: {
        color: "danger",
        text: "Erreur lors de l'ajout de la filiale",
        title: "Ajout"
      }
    },
    update: {
      success: {
        color: "success",
        text: "Filiale modifiée avec succès",
        title: "Mis à jour"
      },
      error: {
        color: "danger",
        text: "Erreur lors de la modification de la filiale",
        title: "Mis à jour"
      }
    },
    delete: {
      success: {
        color: "success",
        text: "Filiale supprimée avec succès",
        title: "Suppression"
      },
      error: {
        color: "danger",
        text: "Erreur lors de la suppression de la filiale",
        title: "Suppression"
      }
    },
    multipleDeletion: {
      success: {
        color: "success",
        text: "Filiales supprimées avec succès",
        title: "Suppression"
      },
      error: {
        color: "danger",
        text: "Erreur lors de la suppression des filiales",
        title: "Suppression"
      }
    },
  },
};
