export default {
  pages: {
    key: "title",
    data: [
      {
        url: "/",
        title: "Accueil",
        is_bookmarked: true,
        icon: "HomeIcon",
      },
      {
        url: "/users",
        title: "Utilisateurs",
        is_bookmarked: true,
        icon: "UsersIcon",
      },
      {
        url: "/workers",
        title: "Employé(e)s",
        is_bookmarked: true,
        icon: "UsersIcon",
      },
      {
        url: "/companies",
        title: "Filiales",
        is_bookmarked: true,
        icon: "LayersIcon",
      },
    ]
  }
}
