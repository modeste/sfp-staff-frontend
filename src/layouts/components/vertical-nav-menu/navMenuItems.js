/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

export default [
  {
    url: "/",
    name: "Accueil",
    slug: "Accueil",
    icon: "HomeIcon"
  },
  {
		url: "/companies",
		name: "Filiales",
		slug: "Filiales",
    icon: "LayersIcon",
    isDisabledAdmin: false,
  },
  {
		url: "/workers",
		name: "Employé(e)s",
		slug: "Employé(e)s",
    icon: "UsersIcon",
    isDisabledAdmin: false,
  },
  {
		url: "/users",
		name: "Utilisateurs",
		slug: "Utilisateurs",
    icon: "UsersIcon",
    isDisabledAdmin: true,
  },
];
