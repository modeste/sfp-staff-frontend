/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
import { AUTH_TOKEN_KEY } from "@/constants";

export default ($http, $config) => {
  const $api = $http.create({
    baseURL: $config.serverURL,
    headers: { "Content-Type": "application/json" }
  });

  $api.interceptors.request.use(
    config => {
      const authToken = sessionStorage.getItem(AUTH_TOKEN_KEY);
      if (authToken) {
        //config.headers["Access-Token"] = sessionStorage.getItem(AUTH_TOKEN_KEY);
        config.headers["Authorization"] = `Bearer ${authToken}`;
      }
      return config;
    },
    error => {
      return Promise.reject(error);
    }
  )

  /**
   * Upload File
   * @param {*} data
   */
  const uploadFile = data => {
    return $api.post("/api/v1/upload-file", data).then(res => res.data);
  };

  /**
   * Login user
   * @param {*} data
   */
  const signin = data => {
    return $api.post('/api/v1/auth/signin', data)
      .then(res => res.data)
  }

  /**
   * Forget password
   * @param {*} data
   */
  const forgetPassword = data => {
    return $api.post('/api/v1/auth/forget-pwd', data)
      .then(res => res.data)
  }

  /**
   * Change password
   * @param {*} data
   */
  const changePassword = data => {
    return $api.post('/api/v1/auth/change-pwd', data)
      .then(res => res.data)
  }

  /**
   * Allow (Active/Inactive) user
   * @param {*} data
   */
  const allowUser = data => {
    return $api.post('/api/v1/users/allow', data)
      .then(res => res.data)
  }

  /**
   * Init password user
   * @param {*} data
   */
  const initializeUser = data => {
    return $api.post('/api/v1/users/initialize', data)
      .then(res => res.data)
  }

  /**
   * Add user
   * @param {*} data
   */
  const addUser = data => {
    return $api.post('/api/v1/users/add', data)
      .then(res => res.data)
  }

  /**
   * Update user
   * @param {*} data
   */
  const putUser = data => {
    return $api.post('/api/v1/users/put', data)
      .then(res => res.data)
  }

  /**
   * Get User
   * @param {*} data
   */
  const getUser = (params) => {
    return $api
      .get('/api/v1/users/item', {
        params
      })
      .then(res => res.data);
  }

  /**
   * Get All Users
   * @param {*} data
   */
  const getAllUsers = (params) => {
    return $api
      .get("/api/v1/users/list", {
        params
      })
      .then(res => res.data);
  }

  /**
   * Remove user
   * @param {*} data
   */
  const removeUser = data => {
    return $api.post('/api/v1/users/remove', data)
      .then(res => res.data)
  }

  const addCompany = data => {
    return $api.post("/api/v1/company/add", data).then(res => res.data)
  };

  const removeCompany = data => {
    return $api.post("/api/v1/company/remove", data).then(res => res.data)
  };

  const putCompany = data => {
    return $api.post("/api/v1/company/put", data).then(res => res.data);
  };

  const getAllCompany = params => {
    return $api
      .get("/api/v1/company/list", {
        params
      }).then(res => res.data);
  };

  const getCompany = params => {
    return $api
      .get("/api/v1/company/item", {
        params
      })
      .then(res => res.data);
  };

  const addWorker = data => {
    return $api.post("/api/v1/worker/add", data).then(res => res.data)
  };

  const removeWorker = data => {
    return $api.post("/api/v1/worker/remove", data).then(res => res.data)
  };

  const putWorker = data => {
    return $api.post("/api/v1/worker/put", data).then(res => res.data);
  };

  const getAllWorker = params => {
    return $api
      .get("/api/v1/worker/list", {
        params
      }).then(res => res.data);
  };

  const getWorker = params => {
    return $api
      .get("/api/v1/worker/item", {
        params
      })
      .then(res => res.data);
  };

  const statsGeneral = params => {
    return $api
      .get("/api/v1/stats/all", {
        params
      })
      .then(res => res.data);
  };

  return {
    uploadFile, signin, forgetPassword, changePassword,
    allowUser, initializeUser, addUser, putUser, removeUser, getUser, getAllUsers,
    addCompany, removeCompany, putCompany, getAllCompany, getCompany, 
    addWorker, removeWorker, putWorker, getAllWorker, getWorker,
    statsGeneral, 
  }
}
