export default (array, chunkVal) => {
    return [].concat.apply([],
        array.map((elem, i) => {
            return i%chunkVal ? [] : [array.slice(i,i+chunkVal)]
        })
    )
}
  