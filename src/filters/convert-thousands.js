export default function (amount) {
  if (!amount || typeof amount !== 'number') {
    return amount
  }

  return amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ')
}
