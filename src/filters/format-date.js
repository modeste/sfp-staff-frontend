import moment from '../helpers/moment'

export default function (date) {
  return moment(new Date(date)).format('D MMMM YYYY')
}
